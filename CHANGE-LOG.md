# Change log

v 1.4.0: initial public version.

v 1.5.0: parse/write of boolean values.

v 1.6.0: when parse fails, exception shows the "partial debug" to help in problem diagnostic.

v 1.7.0: writes flat string for multiple structured field; it's also possible to define a fixed length for the generated flat list.

v 1.8.0: writes and parse LocalDateTime fields. 

v 1.9.0: MapFlatParser converts flat strings to Java maps.

v 1.10.0 Writes lists with fixed size, not only for structured type.

v 1.11.0 parse/write of BigInteger values and Numeric values (for long strings composed by numbers only). 
