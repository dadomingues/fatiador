package fatiador;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;

import com.google.common.base.Strings;

import fatiador.writer.FieldWriter;
import fatiador.writer.FieldWriterFactory;

/**
 * Use this class to convert objects of type T into flat strings.
 */
public class FlatWriter<T> {

    public static final String FIELD_NOT_WRITABLE_PREFIX = "I couldn't read the field ";

    private FlatStructure structure;
    private Class<T> productClass;

    private FieldWriterFactory fieldWriterFactory = new FieldWriterFactory();

    public FlatWriter(FlatStructure structure, Class<T> productClass) {
        this.structure = structure;
        this.productClass = productClass;
    }

    public String write(T bean) {
        try {
            return innerWrite(bean);
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @SuppressWarnings("rawtypes")
    private String innerWrite(T bean) throws Exception {

        StringBuilder flat = new StringBuilder();

        for (FlatField flatField : structure.getFields()) {

            String name = flatField.getName();
            int size = flatField.getSize();
            Field javaField = productClass.getDeclaredField(name);
            javaField.setAccessible(true);

            if (flatField instanceof SimpleField) {
                Object value = javaField.get(bean);
                String flatValue = writeField(value, flatField);
                flat.append(flatValue);
            }

            if (flatField instanceof LengthField) {
                Collection collection = (Collection) javaField.get(bean);
                if (collection == null) {
                    collection = Collections.emptyList();
                }
                Integer value = collection.size();
                String flatValue = Strings.padStart(value.toString(), size, '0');
                flat.append(flatValue);
            }

            if (flatField instanceof MultipleField) {
                Collection collection = collectionFrom(bean, javaField);
                for (Object element : collection) {
                    String flatValue = writeField(element, flatField);
                    flat.append(flatValue);
                }
                String filler = fillerForFixedSizeList(collection, flatField);
                flat.append(filler);
            }
        }

        return flat.toString();
    }

    private Collection collectionFrom(Object bean, Field javaField) throws IllegalAccessException {
        Collection collection = (Collection) javaField.get(bean);
        if (collection == null) {
            collection = Collections.emptyList();
        }
        return collection;
    }

    private String fillerForFixedSizeList(Collection collection, FlatField flatField)
            throws InstantiationException, IllegalAccessException {
        StringBuilder filler = new StringBuilder();
        if (flatField.getListFixedSize() > collection.size()) {
            String defaultFlatValue = flatField.defaultFlatValue();
            for (int i = collection.size(); i < flatField.getListFixedSize(); i++) {
                filler.append(defaultFlatValue);
            }
        }
        return filler.toString();
    }

    private String writeField(Object value, FlatField flatField) {
        FieldWriter fieldWriter = fieldWriterFactory.getFieldWriter(flatField.getFlatType());
        return fieldWriter.write(value, flatField);
    }

}
