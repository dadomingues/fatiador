package fatiador;

public class LengthField extends FlatField {

    public LengthField(String name, int size) {
        super(name, size, FlatType.INTEGER);
    }

}
