package fatiador.writer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeWriter extends AbstractDateTimeWriter<LocalDateTime> {

    @Override
    public String writeDateTime(LocalDateTime value, DateTimeFormatter dateFormat) {
        return value.format(dateFormat);
    }

}
