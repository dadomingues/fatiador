package fatiador.writer;

import com.google.common.base.Strings;

import fatiador.FlatField;

public class DecimalWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) {
        int decimalDigitsSize = flatField.getDecimalDigitsSize();
        Double doubleValue = value != null ? (Double) value : 0;
        String integerPart = doubleValue.toString().split("\\.")[0];
        String decimalPart = doubleValue.toString().split("\\.")[1];
        int size = flatField.getSize();
        integerPart = Strings.padStart(integerPart, size - decimalDigitsSize, '0');
        decimalPart = Strings.padEnd(decimalPart, decimalDigitsSize, '0');
        decimalPart = decimalPart.substring(0, decimalDigitsSize);
        return integerPart + decimalPart;
    }

}
