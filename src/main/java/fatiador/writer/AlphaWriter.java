package fatiador.writer;

import com.google.common.base.Strings;

import fatiador.FlatField;

public class AlphaWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) {
        int size = flatField.getSize();
        return Strings.padEnd(Strings.nullToEmpty((String) value), size, ' ');
    }

}
