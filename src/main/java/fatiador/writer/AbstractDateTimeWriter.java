package fatiador.writer;

import java.time.format.DateTimeFormatter;
import java.time.temporal.UnsupportedTemporalTypeException;

import com.google.common.base.Strings;

import fatiador.FlatField;

public abstract class AbstractDateTimeWriter<T> implements FieldWriter {

    private static final String FIELD_NOT_WRITABLE = "I couldn't write the field [%s] with the value [%s] using the pattern [%s].";

    @Override
    public String write(Object value, FlatField flatField) {
        String pattern = flatField.getDateTimeFormat();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(pattern);
        String flatValue;
        if (value != null) {
            try {
                flatValue = writeDateTime((T) value, dateFormat);
            } catch (UnsupportedTemporalTypeException e) {
                String msg = String.format(FIELD_NOT_WRITABLE, flatField.getName(), value, pattern);
                throw new IllegalArgumentException(msg, e);
            }
        } else {
            flatValue = Strings.padEnd("", pattern.length(), ' ');
        }
        return flatValue;
    }

    public abstract String writeDateTime(T value, DateTimeFormatter dateFormat);

}
