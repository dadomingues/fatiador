package fatiador.writer;

import fatiador.FlatField;

public interface FieldWriter {

    String write(Object value, FlatField flatField);
    
}
