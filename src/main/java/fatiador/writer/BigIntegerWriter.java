package fatiador.writer;

import com.google.common.base.Strings;
import fatiador.FlatField;

import java.math.BigInteger;

public class BigIntegerWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) {
        int size = flatField.getSize();
        BigInteger intValue = value != null ? (BigInteger) value : BigInteger.valueOf(0);
        return Strings.padStart(intValue.toString(), size, '0');
    }

}
