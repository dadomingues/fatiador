package fatiador.writer;

import com.google.common.base.Strings;

import fatiador.FlatField;

public class IntegerWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) {
        int size = flatField.getSize();
        Integer intValue = value != null ? (Integer) value : 0;
        return Strings.padStart(intValue.toString(), size, '0');
    }

}
