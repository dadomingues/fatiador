package fatiador.writer;

import fatiador.FlatField;
import fatiador.FlatWriter;

public class StructuredWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) {
        FlatWriter elementWriter = new FlatWriter(flatField.getStructure(), flatField.getBeanClass());
        return elementWriter.write(value);
    }

}
