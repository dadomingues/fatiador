package fatiador.writer;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeWriter extends AbstractDateTimeWriter<LocalTime> {

    @Override
    public String writeDateTime(LocalTime value, DateTimeFormatter dateFormat) {
        return value.format(dateFormat);
    }

}
