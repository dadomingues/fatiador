package fatiador.parser;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateParser extends AbstractDateTimeParser<LocalDate> {

    @Override
    public LocalDate parseDateTime(String rawValue, DateTimeFormatter dateFormat) {
        return LocalDate.parse(rawValue, dateFormat);
    }

}
