package fatiador.parser;

import java.util.HashMap;
import java.util.Map;

import fatiador.FlatStructure;

/**
 * Parses flat string to Java map.
 *
 */
public class MapFlatParser extends AbstractFlatParser<Map<String, String>> {

    private Map<String, String> valuesMap;

    public MapFlatParser(FlatStructure structure) {
        super(structure);
    }

    @Override
    public void setValue(String fieldName, Object value) {
        valuesMap.put(fieldName, value.toString());
    }

    @Override
    public Map<String, String> getBean() {
        return valuesMap;
    }

    @Override
    public void addInCollection(String fieldName, Object value) {
        if (valuesMap.get(fieldName) == null) {
            valuesMap.put(fieldName, value.toString());
        } else {
            String previousValue = valuesMap.get(fieldName);
            valuesMap.put(fieldName, previousValue + ", " + value.toString());
        }
    }

    @Override
    public void initializeBean() {
        valuesMap = new HashMap<>();        
    }

}
