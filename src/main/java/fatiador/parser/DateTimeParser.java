package fatiador.parser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeParser extends AbstractDateTimeParser<LocalDateTime> {

    @Override
    public LocalDateTime parseDateTime(String rawValue, DateTimeFormatter dateFormat) {
        return LocalDateTime.parse(rawValue, dateFormat);
    }

}
