package fatiador.parser;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeParser extends AbstractDateTimeParser<LocalTime> {

    @Override
    public LocalTime parseDateTime(String rawValue, DateTimeFormatter dateFormat) {
        return LocalTime.parse(rawValue, dateFormat);
    }

}
