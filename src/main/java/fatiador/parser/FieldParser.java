package fatiador.parser;

import fatiador.FlatField;
import fatiador.ParserArgumentException;

public interface FieldParser<T> {

    T parse(String rawValue, FlatField flatField) throws ParserArgumentException;

}
