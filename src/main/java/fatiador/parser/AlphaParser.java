package fatiador.parser;

import fatiador.FlatField;
import fatiador.ParserArgumentException;

public class AlphaParser implements FieldParser<String> {

    @Override
    public String parse(String rawValue, FlatField flatField) throws ParserArgumentException {
        return rawValue.trim();
    }

}
