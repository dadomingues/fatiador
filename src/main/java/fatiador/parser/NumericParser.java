package fatiador.parser;

import fatiador.FlatField;
import fatiador.ParserArgumentException;

/**
 * Numeric Type parse values with spaces at the beginning and at the end
 */
public class NumericParser implements FieldParser<String> {

    private static final String ERROR_MESSAGE = "Field [%s] not parsable. Value [%s] not convertible to Numeric.";

    @Override
    public String parse(String rawValue, FlatField flatField) throws ParserArgumentException {
        rawValue = rawValue.trim();
        if (!rawValue.matches("^[0-9]+$")) {
            String msg = String.format(ERROR_MESSAGE, flatField.getName(), rawValue);
            throw new ParserArgumentException(msg);
        }
        return rawValue;
    }

}
