package fatiador.parser;

import fatiador.FlatType;

public class FieldParserFactory {

    public FieldParser<?> getFieldParser(FlatType flatType) {
        switch (flatType) {
        case ALPHA:
            return new AlphaParser();
        case NUMERIC:
            return new NumericParser();
        case INTEGER:
            return new IntegerParser();
        case BIG_INTEGER:
            return new BigIntegerParser();
        case DECIMAL:
            return new DecimalParser();
        case BOOLEAN:
            return new BooleanParser();
        case DATE:
            return new DateParser();
        case TIME:
            return new TimeParser();
        case DATE_TIME:
            return new DateTimeParser();
        case STRUCTURED:
            return new StructuredParser();
        default:
            throw new IllegalArgumentException("No known parser for type " + flatType);
        }
    }

}
