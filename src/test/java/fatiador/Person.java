package fatiador;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Person {

    public String identificationNumber;
    public String name;
    public LocalDate birthday;
    public LocalTime birthTime;
    public LocalDateTime birthDateTime;
    public Integer age;
    public List<String> dogNames = new ArrayList<>();
    public List<Integer> scores = new ArrayList<>();
    public Double weight;
    public List<Dog> dogs = new ArrayList<>();
    public Boolean isAdult;

    public Person() {

    }

    public Person(String identificationNumber, String name) {
        this.identificationNumber = identificationNumber;
        this.name = name;
    }

    public Person(String identificationNumber, String name, Integer age) {
        this.identificationNumber = identificationNumber;
        this.name = name;
        this.age = age;
    }

    public Person(String identificationNumber, String name, double weight) {
        this.identificationNumber = identificationNumber;
        this.name = name;
        this.weight = weight;
    }

    public Person(String identificationNumber, String name, int age, double weight) {
        this.identificationNumber = identificationNumber;
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public void setScores(Integer... scores) {
        this.scores = Arrays.asList(scores);
    }

    public void setDogs(String... dogs) {
        this.dogNames = Arrays.asList(dogs);
    }

    @Override
    public boolean equals(final Object other) {
        if (other == null) {
            return false;
        }
        if (!getClass().equals(other.getClass())) {
            return false;
        }
        Person castOther = (Person) other;
        return new EqualsBuilder().append(identificationNumber, castOther.identificationNumber)
                .append(name, castOther.name).append(birthday, castOther.birthday)
                .append(birthTime, castOther.birthTime).append(birthDateTime, castOther.birthDateTime)
                .append(age, castOther.age).append(dogNames, castOther.dogNames).append(scores, castOther.scores)
                .append(weight, castOther.weight).append(dogs, castOther.dogs).append(isAdult, castOther.isAdult)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(identificationNumber).append(name).append(birthday).append(birthTime)
                .append(birthDateTime).append(age).append(dogNames).append(scores).append(weight).append(dogs)
                .append(isAdult).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("identificationNumber", identificationNumber).append("name", name)
                .append("birthday", birthday).append("birthTime", birthTime).append("birthDateTime", birthDateTime)
                .append("age", age).append("dogNames", dogNames).append("scores", scores).append("weight", weight)
                .append("dogs", dogs).append("isAdult", isAdult).toString();
    }

}
