package fatiador;

import static org.junit.Assert.*;

import org.junit.Test;

public class FlatFieldTest {

    @Test
    public void integer_default_value() {
        FlatField field = new SimpleField("field", 5, FlatType.INTEGER);
        String defaultFlatValue = field.defaultFlatValue();
        assertEquals("00000", defaultFlatValue);
    }

    @Test
    public void big_integer_default_value() {
        FlatField field = new SimpleField("field", 12, FlatType.BIG_INTEGER);
        String defaultFlatValue = field.defaultFlatValue();
        assertEquals("000000000000", defaultFlatValue);
    }
    
    @Test
    public void decimal_default_value() {
        FlatField field = new SimpleField("field", 5, FlatType.DECIMAL);
        String defaultFlatValue = field.defaultFlatValue();
        assertEquals("00000", defaultFlatValue);
    }

    @Test
    public void numeric_default_value() {
        FlatField field = new SimpleField("field", 50, FlatType.NUMERIC);
        String defaultFlatValue = field.defaultFlatValue();
        assertEquals("00000000000000000000000000000000000000000000000000", defaultFlatValue);
    }
    
    @Test
    public void alpha_default_value() {
        FlatField field = new SimpleField("field", 5, FlatType.ALPHA);
        String defaultFlatValue = field.defaultFlatValue();
        assertEquals("     ", defaultFlatValue);
    }

    @Test
    public void structured_default_value() {
        
        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addSimpleAlphaField("name", 5);
        dogStructure.addSimpleIntegerField("age", 2);
        FlatField field = new MultipleField("dogs", dogStructure, Dog.class);
        
        String defaultFlatValue = field.defaultFlatValue();
        assertEquals("     00", defaultFlatValue);
    }

}
